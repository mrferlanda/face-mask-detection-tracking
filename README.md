# Face Mask Detection

# How to Use

### Training the Model (Google Colab)

![Colab](static/1.png)
I have provided colab link to train the model, you just need to simply run all of the cells and download the result (best.pt) file. Don't forget to create account on W&B and pass the key for live training viz result. <br>
Colab Link: [Click Here](https://colab.research.google.com/drive/1hEB5P2tsP76jJNkiUk6MCZ0mL6asTGki)

## Running the Inference (detect.py)

Run this code for inference: 
```
$python yolov5/detect.py --weights yolov5/best.pt --img 416   --source 0              #webcam
                                                                       img.jpg        # image
                                                                       vid.mp4        # video
                                                                       path/          # directory
                                                                       path/*.jpg     # glob
                                                                       'https://youtu.be/Zgi9g1ksQHc'  # YouTube
                                                                       'rtsp://example.com/media.mp4'  # RTSP, RTMP, HTTP stream
```

## Running the Inference using Flask (GUI)

Run this code to start the flask:<br>
`flask run`

By default, you can access the app on port 5000:
![cli](static/2.png) <br><br>
If everything is flawless, you will be able to view this interface: 
![gui](static/3.png)

Simply just upload your image/video into the choose file direction, and you will get the result back.

## Running the Inference using Docker

Pull the image on docker by using this code: <br>
`docker pull mrferlanda/facemask-detection`

After that just run: <br>
`docker run -p 5000:5000 mrferlanda/facemask-detection` <br>
After that just access port 5000 on your localhost.

# Result

![inp2](static/inp_2.png)
![res2](static/res_2.png)

# Reference 

* YOLOv5 by Ultralytics
* https://public.roboflow.com/object-detection/mask-wearing

